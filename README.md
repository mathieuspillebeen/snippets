# Canvas
## To convert snippets from percentages to points on 20:
Insert this javascript in the browser's inspector:
```javascript
document.getElementsByClassName("viewport_1")[0].addEventListener('scroll', (evt) => { 
  var grades = document.querySelectorAll(".percentage"); 
  var controleTotaal = 0; 
  for (var i = 0; i < grades.length; i++) { 
    var percentageCell = grades.item(i); 
    var percentage = grades.item(i).innerText.replace(',', '.'); 
    
    if (!isNaN(Math.round(parseFloat(percentage) / 5))) { 
      var rounded_number = Math.round(parseFloat(percentage) / 5); 
      grades.item(i).innerHTML = parseFloat(percentage) + '% means ' + rounded_number; 
      controleTotaal = controleTotaal + rounded_number; 
    } 
  }
  
  console.log('controle Totaal is ' + controleTotaal);
});
```

# Ibamaflex
Fill "ND0" overal in
```javascript
var grades = document.querySelectorAll('input[name*="txtScore"]');
for (var i = 0; i < grades.length; i++) {
  grades.item(i).value = 'ND0';
}
```

Use ffmpeg to convert gif to mp4
Firstly install ffmpeg:
```brew install ffmpeg```

Next convert any gif file to mp4 using this command: 
```ffmpeg -i animated.gif -pix_fmt yuv420p output.mp4```

